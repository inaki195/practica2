package com.ifb.inaki.orther;

import com.ifb.inaki.mvc.Controlador;
import com.ifb.inaki.mvc.Modelo;
import com.ifb.inaki.mvc.Vista;

public class Principal {
    /**
     * meetodo que ejecuta el programa
     * @param args String[]
     */
    public static void main(String[] args) {
        Vista vista=new Vista();
        Modelo modelo=new Modelo();
        Controlador controlador=new Controlador(vista,modelo);
    }
}
