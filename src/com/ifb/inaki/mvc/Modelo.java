package com.ifb.inaki.mvc;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class Modelo {
    private Connection conexion;

    /**
     * conecta con base de datos
     * conecta con la base de Policias por defecto
     * @throws SQLException sql
     */
    public void conectar() throws SQLException {
        conexion=null;
        conexion= DriverManager.getConnection("jdbc:mysql://localhost:3306/Policias",
                "root","");
    }

    /**
     * deconectar la conexion con  la base de datos
     * @throws SQLException sql
     */
    public void desconectar() throws SQLException {
        conexion.close();
        conexion=null;
    }

    /**
     * cargar los datos de la base de datos y te los muestra en el dtm
     * @return ResultSet
     * @throws SQLException sql
     */
    public ResultSet obtenerDatos() throws SQLException {
        if(conexion==null){
            return  null;
        }
        if(conexion.isClosed()){
            return null;
        }
        String consulta ="SELECT * FROM polis";
        PreparedStatement sentencia = null;
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * metodo que te permite add un policia a la tabla de datos
     * @param nombre {@link String}
     * @param apellido {@link String}
     * @param rango {@link String}
     * @param fechaIncorporacion {@link LocalDate}
     * @param sueldo {@link Float}
      @return int {@link Integer}
      @throws SQLException sql
     */
    public int insertarPolica(String nombre, String apellido, String rango,
                              LocalDate fechaIncorporacion, float sueldo) throws SQLException {
        if(conexion==null){
            return -1;
        }
        if(conexion.isClosed()){
            return -2;
        }
        String consulta="INSERT INTO polis(nombre,apellido,rango,fecha_ingresos,sueldo) values(?,?,?,?,?)";
        PreparedStatement sentencia=conexion.prepareStatement(consulta);
        sentencia.setString(1,nombre);
        sentencia.setString(2,apellido);
        sentencia.setString(3,rango);
        sentencia.setDate(4, Date.valueOf(fechaIncorporacion));
        sentencia.setFloat(5,sueldo);

        int numeroResgistros=sentencia.executeUpdate();
        if(sentencia!=null){
            sentencia.close();
        }
        return numeroResgistros;
    }

    /**
     * te permite borrar un elemento completo de la base de datos
     * @param id {@link Integer}
     * @return int
     * @throws SQLException sql
     */
    public int eliminarPolicias(int id) throws SQLException {
        if (conexion==null) {
            return -1;
        }
        if (conexion.isClosed()) {
            return -2;
        }
        String consulta="delete from polis where id=?";
        PreparedStatement sentencia=null;
        sentencia=conexion.prepareStatement(consulta);
        sentencia.setInt(1,id);
        int resultado=sentencia.executeUpdate();
        if(sentencia!=null){
            sentencia.close();
        }
        return resultado;
    }

    /**
     * metodo que te permite modificar campos de la base de dato
     * @param id {@link Integer}
     * @param nombre {@link String}
     * @param apellido {@link String}
     * @param rango {@link String}
     * @param fechaIncorporacion LocalDate.
     * @param sueldo {@link Double}
     * @return Integer
     * @throws SQLException sql
     */
    public int modificarPoli(int id, String nombre, String apellido, String rango,
                             LocalDate fechaIncorporacion, Double sueldo    ) throws SQLException {
        String consulta="update polis set nombre=?,apellido=?,rango=?,fecha_ingresos=?,sueldo=? where id=?";
        PreparedStatement sentencia=null;
        sentencia=conexion.prepareStatement(consulta);
        sentencia.setInt(6,id);
        sentencia.setString(1,nombre);
        sentencia.setString(2,apellido);
        sentencia.setString(3,rango);
        sentencia.setDate(4, java.sql.Date.valueOf((fechaIncorporacion)));
        sentencia.setDouble(5,sueldo);
        int resultado=sentencia.executeUpdate();
        if (sentencia==null){
            sentencia.close();
        }
        return resultado;
    }

    /**
     * cambia el dtm y te muestra cierto
     * @param palabra {@link String}
     * @return ResultSet
     */
    public ResultSet bucarPersonal(String palabra) {
        ResultSet resultado = null;
        try {
            System.out.println(palabra);
            String consulta="SELECT * from polis where nombre like '%"+palabra+"%'";
            PreparedStatement sentencia=null;
            sentencia=conexion.prepareStatement(consulta);
             resultado=sentencia.executeQuery();
            return resultado;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultado;
    }

}
