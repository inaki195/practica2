package com.ifb.inaki.mvc;

import com.ifb.inaki.JConecta;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;


public class Controlador implements ActionListener,TableModelListener {
    private Vista vista;
    private Modelo modelo;

    @Override
    /**
     * modica la ta tabla
     */
    public void tableChanged(TableModelEvent e) {
        if(e.getType()==TableModelEvent.UPDATE){
            int filamodificada=e.getFirstRow();
           
            try {
                modelo.modificarPoli(
                        (Integer) vista.dtm.getValueAt(filamodificada,0),
                        (String)vista.dtm.getValueAt(filamodificada,1),
                        (String)vista.dtm.getValueAt(filamodificada,2),
                        (String)vista.dtm.getValueAt(filamodificada,3),
                        LocalDate.parse(String.valueOf(vista.dtm.getValueAt(filamodificada,4))),
                        Double.parseDouble(String.valueOf(vista.dtm.getValueAt(filamodificada,5))));
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

    private enum tipoEstado  {conectado,desconectado};
    private tipoEstado estado;

    /**
     * comunica la clase vista con la clase modelo
     * @param vista {@link Object}
     * @param modelo {@link Object}
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;
        estado=tipoEstado.desconectado;
        iniciarTabla();
        addActionvListener(this);
        addTableModelListerner(this);
    }

    /**
     * add listener a la tabla
     * @param listener {@link TableModelListener}
     */
    private void addTableModelListerner(TableModelListener listener) {
        vista.dtm.addTableModelListener(listener);
    }

    /**
     * add listener a la barras de menu y a los botones
     * @param listener {@link ActionListener}
     */
    private void addActionvListener(ActionListener listener) {
        vista.btnBuscar.addActionListener(listener);
        vista.btnEliminar.addActionListener(listener);
        vista.btnNuevo.addActionListener(listener);
        vista.itemConectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.intemConfiguracion.addActionListener(listener);
    }


    /**
     * te pone la cabecera de los campos de las tablas
     */
    private void iniciarTabla() {
        String[] header={"nombre","apallido","rango","fecha","sueldo","id"};
        vista.dtm.setColumnIdentifiers(header);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando=e.getActionCommand();
        switch (comando){
            case "nuevo":{
                try {
                    modelo.insertarPolica(vista.txtnombre.getText(), vista.txtapellido.getText(),
                            vista.txtRango.getText(),vista.fechaIngreso.getDate(),Float.parseFloat(vista.txtSueldo.getText()));
                    cargarFilas(modelo.obtenerDatos());
                    limpiarCampos();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            break;
            case "eliminar":{
                int filaBorrar=vista.tabla.getSelectedRow();
                int idBorrar= (int) vista.dtm.getValueAt(filaBorrar,0);
                try {
                    modelo.eliminarPolicias(idBorrar);
                    vista.dtm.removeRow(filaBorrar);
                    cargarFilas(modelo.obtenerDatos());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            break;
            case"Conectar":{

                if(estado==tipoEstado.desconectado){
                    try {
                        modelo.conectar();
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                        vista.btnEstado.setText("conectado");
                        vista.btnEstado.setBackground(Color.green);


                    vista.itemConectar.setText("Desconectar");

                        try {
                            cargarFilas(modelo.obtenerDatos());

                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }

                }
            }
            case"Salir":{

            }
            case"configuracion":{
                JConecta conecta=new JConecta();
            }
            case"buscar":{
                String palabra=vista.txtbuscar.getText();
                if(estado==tipoEstado.conectado){
                try {
                    cargarFilas(modelo.bucarPersonal(palabra));
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

            }
        }
    }




}

    /**
     * limpia los campos
     */
    private void limpiarCampos(){
        vista.txtnombre.setText("");
        vista.txtapellido.setText("");
        vista.txtRango.setText("");
        vista.txtSueldo.setText("");
        vista.fechaIngreso.setText("");
    }

    /**
     * carga en el dtm y muestra los datos de la base de datos
     * @param resultSet resultado
     * @throws SQLException sql
     */
    private void cargarFilas(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[6];
        vista.dtm.setRowCount(0);
        while (resultSet.next()) {
            fila[0] = resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);
            vista.dtm.addRow(fila);
        }
        if(resultSet.last()){

        }
    }
    }
