package com.ifb.inaki.mvc;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Vista {
    private JPanel panel1;
     JButton btnBuscar;
     JButton btnNuevo;
     JButton btnEliminar;
     JTextField txtnombre;
     JTextField txtapellido;
     JTable tabla;
     JTextField txtRango;
     JTextField txtSueldo;
     JTextField txtbuscar;
     DatePicker fechaIngreso;
     JButton btnEstado;
    DefaultTableModel dtm;
    JFrame frame;
    JMenuItem itemConectar;
    JMenuItem itemSalir;
    JMenuItem intemConfiguracion;

    /**
     * constructor ejecuta la ventana
     */
    public Vista() {
         frame = new JFrame("policias");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        dtm=new DefaultTableModel();
        tabla.setModel(dtm);
        crearmenu();
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * te crea el menu
     */
    private void crearmenu() {
        intemConfiguracion=new JMenuItem("configuracion");
        intemConfiguracion.setActionCommand("configuracion");
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        JMenu menuArchivo = new JMenu("Archivo");
        JMenu Configuracion=new JMenu("configuracion");

        menuArchivo.add(itemConectar);
        menuArchivo.add(itemSalir);
        Configuracion.add(intemConfiguracion);
        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);
        barraMenu.add(Configuracion);
        frame.setJMenuBar(barraMenu);
    }


}
