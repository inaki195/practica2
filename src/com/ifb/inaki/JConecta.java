package com.ifb.inaki;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JConecta  extends JDialog {
    private JPanel panel1;
    private JTextField tfUsuario;
    private JPasswordField tfContrasena;
    private JButton btConectar;
    private JTextField tfHost;
    private JButton btCancelar;
    private JComboBox<String> cbCatalogo;
    private JTextField txtpuerto;
    boolean conectado;
    private String host;
    private String usuario;
    private String contrasena;
    private String catalogo;

    public enum Accion {
        ACEPTAR, CANCELAR
    }

    private Accion accion;
    /**
     * constructor del dialogo
     */
    public JConecta() {
        setContentPane(panel1);
        pack();
        setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        setModal(true);
        setLocationRelativeTo(null);

        btConectar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                aceptar();
            }
        });

        btCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelar();
            }
        });

        cbCatalogo.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                listarCatalogo();
            }
        });
        setVisible(true);
    }

    /**
     * invisibiliza el dialog
     */
    private void cancelar() {
        accion = Accion.CANCELAR;
        setVisible(false);
    }

    /**
     * conecta la base de datos con los parametros que le pasas host ,users ,puerto ,base de datos
     */
    private void aceptar() {
        host = tfHost.getText();
        usuario = tfUsuario.getText();
        contrasena = String.valueOf(tfContrasena.getPassword());
        catalogo = (String) cbCatalogo.getSelectedItem();

        accion = Accion.ACEPTAR;
        setVisible(false);
        conectado=true;

    }

    /**
     * te lista todas la bases de datos que halla
     */
    private void listarCatalogo() {
        if (cbCatalogo.getItemCount() > 0)
            return;

        Connection conexion = null;

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conexion = DriverManager.getConnection("jdbc:mysql://" +
                            tfHost.getText()
                            + ":"+txtpuerto.getText()
                    , tfUsuario.getText(),
                    String.valueOf(tfContrasena.getPassword()));

            // Obtiene información sobre las Bases de Datos que hay en el servidor
            ResultSet catalogo = conexion.getMetaData().getCatalogs();

            while (catalogo.next()) {
                cbCatalogo.addItem(catalogo.getString(1));
            }
            catalogo.close();

        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } catch (InstantiationException ie) {
            ie.printStackTrace();
        } catch (IllegalAccessException iae) {
            iae.printStackTrace();
        }
    }

    public JTextField getTfUsuario() {
        return tfUsuario;
    }

    public void setTfUsuario(JTextField tfUsuario) {
        this.tfUsuario = tfUsuario;
    }

    public JPasswordField getTfContrasena() {
        return tfContrasena;
    }

    public void setTfContrasena(JPasswordField tfContrasena) {
        this.tfContrasena = tfContrasena;
    }

    public JTextField getTxtpuerto() {
        return txtpuerto;
    }

    public void setTxtpuerto(JTextField txtpuerto) {
        this.txtpuerto = txtpuerto;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getCatalogo() {
        return catalogo;
    }

    public boolean isConectado() {
        return conectado;
    }

    public void setConectado(boolean conectado) {
        this.conectado = conectado;
    }

    public void setCatalogo(String catalogo) {
        this.catalogo = catalogo;
    }
}