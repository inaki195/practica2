

create database Policias;

create table polis(
id int primary key auto_increment not null,
nombre varchar(22),
apellido varchar(23),
rango varchar(15),
fecha_ingresos date,
sueldo real
);
